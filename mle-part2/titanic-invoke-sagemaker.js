var AWS = require('aws-sdk');

exports.handler = (event) => {
    const sagemakerruntime = new AWS.SageMakerRuntime({region: 'us-east-1'});
    let csv = Object.values(event).join(",");

    const params = {
        Body: csv,
        EndpointName: process.env.ENDPOINT_NAME,
        ContentType: 'text/csv',
        Accept: 'text/csv'
    };

    return new Promise((resolve, reject) => {
        return sagemakerruntime.invokeEndpoint(params, (err, data) => {
            if (err) {
                let response = {
                    statusCode: 400,
                    body: JSON.stringify(err),
                };
                reject(response);
            }
            else {
                let survived = 'Yes'
                if (JSON.parse(Buffer.from(data.Body).toString('utf8')) === 0) {
                    survived = 'No'
                }
                
                let response = {
                    statusCode: 200,
                    survived: survived
                };
                resolve(response);
            }
        });
    });
};