import * as rp from 'request-promise-native';
import * as awsSdk from 'aws-sdk';


exports.search = async (event) => {
    const s3 = new awsSdk.S3();
    let searchKey = event.pathParameters['key'];
    const SFDCUrl = `https://api.status.salesforce.com/v1/search/${searchKey}`;

    return new Promise((resolve, reject) => {
        rp(SFDCUrl)
        .then((response) => {
            s3.putObject({
                Bucket: process.env.S3_SEARCH_BUCKET,
                Key: `${searchKey}.json`,
                Body: response,
            }).promise().then((s3response) => {
                resolve(successResponse(response))
            }).catch((s3Error) => {
                reject(failResponse(s3Error));
            })
        })
        .catch((err) => {
            reject(failResponse(err));
        });
    });
}

exports.weather = async (event) => {
    const s3 = new awsSdk.S3();
    let weatherKey = event.pathParameters['key'];
    const weatherURL = `http://samples.openweathermap.org/data/2.5/weather?q=${weatherKey}&appid=b6907d289e10d714a6e88b30761fae22`;

    return new Promise((resolve, reject) => {
        rp(weatherURL)
        .then((response) => {
            s3.putObject({
                Bucket: process.env.S3_WEATHER_BUCKET,
                Key: `${weatherKey}.json`,
                Body: response,
            }).promise().then((s3response) => {
                resolve(successResponse(response))
            }).catch((s3Error) => {
                reject(failResponse(s3Error));
            })
        })
        .catch((err) => {
            reject(failResponse(err));
        });
    });
}

const successResponse = (body: any) => {
    return {
        statusCode: 200,
        body: JSON.stringify(body)
    }
}

const failResponse = (body: any) => {
    return {
        statusCode: 400,
        body: JSON.stringify(body)
    }
}
